using System;
using System.IO;
using System.Linq;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;

namespace homework_9_4
{
    public class TelegramBot
    {
        private static ITelegramBotClient botClient;

        public static void Start()
        {
            botClient = new TelegramBotClient("1870733736:AAGH9R395Zh-RQcfKPvyFrXE_ZTe9963fwI");

            botClient.OnMessage += Bot_OnMessage;
            botClient.StartReceiving();

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

            botClient.StopReceiving();
        }

        private static async void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            Console.WriteLine(e.Message.Type);
            Console.WriteLine(e.Message.Text);

            if (e.Message.Text != null)
            {
                if (e.Message.Text == "/start")
                    await botClient.SendTextMessageAsync(
                        e.Message.Chat,
                        "Добро пожаловать, " + e.Message.From.FirstName
                    );

                if (e.Message.Text == "/datetime")
                    await botClient.SendTextMessageAsync(
                        e.Message.Chat,
                        DateTime.Now.ToString()
                    );

                if (e.Message.Text == "/files")
                {
                    var directoryInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
                    var files = directoryInfo.GetFileSystemInfos().Where(file => file.Name.StartsWith(" "))
                        .Select(file => file.Name).ToList();
                    await botClient.SendTextMessageAsync(
                        e.Message.Chat,
                        files.Count == 0
                            ? "Нет загруженных файлов"
                            : "Список загруженных файлов. Для получения файла отправьте сообщение с текстом <file имя_файла>\n" +
                              string.Join(",\n", files)
                    );
                }

                if (e.Message.Text.StartsWith("file "))
                {
                    var fileName = e.Message.Text.Split(" ")[1];
                    if (File.Exists(" " + fileName))
                    {
                        await using var stream = File.Open(" " + fileName, FileMode.Open);
                        await botClient.SendDocumentAsync(e.Message.Chat, stream, null, fileName); 
                    }
                    else
                    {
                        await botClient.SendTextMessageAsync(e.Message.Chat, "Такой файл не существует");
                    }
                }
            }
            else if (e.Message.Type == MessageType.Audio)
            {
                Download(e.Message.Audio.FileId, e.Message.Audio.FileName);
            }
            else if (e.Message.Type == MessageType.Video)
            {
                Download(e.Message.Video.FileId, e.Message.Video.FileName);
            }
            else if (e.Message.Type == MessageType.Document)
            {
                Download(e.Message.Document.FileId, e.Message.Document.FileName);
            }
        }

        private static async void Download(string fileId, string path)
        {
            var file = await botClient.GetFileAsync(fileId);
            var fs = new FileStream(" " + path, FileMode.Create);
            await botClient.DownloadFileAsync(file.FilePath, fs);
            fs.Close();
            fs.Dispose();
        }
    }
}